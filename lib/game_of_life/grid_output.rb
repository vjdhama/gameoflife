class GridOutput
  attr_reader :grid

  def initialize(grid)
    @grid = grid
  end

  def canvas_output   
    puts @grid
  end
end