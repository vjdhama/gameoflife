require 'spec_helper'

describe GridOutput do
  describe "#canvas_output" do
    it "should print to stdout" do   
      grid = [['*','0','0'],['0','*','0'],['0','0','*']]
      grid_output = GridOutput.new(grid)
      expect { grid_output.canvas_output }.to output.to_stdout
    end
  end
end